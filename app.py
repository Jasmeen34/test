import connexion
from connexion import NoContent
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datetime
from base import Base
import requests
from stats import Stats
import yaml
import logging
import logging.config
import json
from apscheduler.schedulers.background import BackgroundScheduler
import sqlalchemy as db
from flask_cors import CORS, cross_origin
from swagger_ui_bundle import swagger_ui_path
import os
from os.path import exists
import sqlite3


def create_database(path):
    
    conn = sqlite3.connect('stats.sqlite')
    c = conn.cursor()
    c.execute('''
          CREATE TABLE stats
          (total_reports INTEGER NOT NULL,
           num_bp INTEGER NOT NULL,
           num_hr INTEGER NOT NULL,
           num_bp_alerts INTEGER NOT NULL,
           num_hr_alerts INTEGER NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

    conn.commit()
    conn.close()

with open("app_conf.yml", 'r') as f:
    app_config = yaml.safe_load(f.read())

with open("log_conf.yml", 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)
PATH = app_config['sqlite']['path']
isExist = os.path.exists(PATH)
if isExist == True:
    print("Exists")
else:
    create_database(PATH)

logger = logging.getLogger('basicLogger')
DB_ENGINE = create_engine(f"sqlite:///{PATH}")
#DB_ENGINE = create_engine("sqlite:///%s" %app_config["datastore"]["stats.sqlite"])
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)
def report_blood_pressure(body):
    headers = {"content-type": "application/json"}
    session = DB_SESSION()
    results = session.query(Stats)
    patient_id = body['patient_id']
    device_id = body['device_id']
    diastolic = body['blood_pressure']['diastolic']
    systolic = body['blood_pressure']['systolic']
    try:
        num_bp_alerts = results[-1].num_bp_alerts
    except IndexError:
        num_bp_alerts = 0
    try:
        total_reports = results[-1].total_reports
    except IndexError:
        total_reports = 0
    try:
        num_bp = results[-1].num_bp
    except IndexError:
        num_bp = 0
    try:
        num_hr = results[-1].num_hr
    except IndexError:
        num_hr = 0
    try:
        num_hr_alerts = results[-1].num_hr_alerts
    except IndexError:
        num_hr_alerts = 0
    logger.info(f'requested data {body}' )

    if body['blood_pressure']['diastolic'] > app_config['diastolic_alert_value']['value']:
        logger.info(f'diastolic BP for {patient_id} from device {device_id} is too high {diastolic}' )
        num_bp_alerts +=1
    if body['blood_pressure']['systolic'] > app_config['systolic_alert_value']['value']:
        logger.info(f'Systolic BP for {patient_id} from device {device_id} is too high {systolic}' )
        num_bp_alerts +=1
    total_reports +=1
    num_bp +=1
    stats = Stats(total_reports,
        num_bp,
        num_hr,
        num_bp_alerts,
        num_hr_alerts)

    session.add(stats)

    session.commit()
    session.close()
    logger.info('BP recorded')
    return 201

def report_heart_rate(body):
    headers = {"content-type": "application/json"}
    patient_id = body['patient_id']
    device_id = body['device_id']
    heart_rate = body['heart_rate']
    session = DB_SESSION()
    results = session.query(Stats)
    try:
        num_bp_alerts = results[-1].num_bp_alerts
    except IndexError:
        num_bp_alerts = 0
    try:
        total_reports = results[-1].total_reports
    except IndexError:
        total_reports = 0
    try:
        num_bp = results[-1].num_bp
    except IndexError:
        num_bp = 0
    try:
        num_hr = results[-1].num_hr
    except IndexError:
        num_hr = 0
    try:
        num_hr_alerts = results[-1].num_hr_alerts
    except IndexError:
        num_hr_alerts = 0
    logger.info(f'requested data {body}' )
    if body['heart_rate']> app_config['heart_rate_alert_value']['value']:
        logger.info(f'heart reate for {patient_id} from device {device_id} is too high {heart_rate}' )
        num_hr_alerts +=1
    total_reports +=1
    num_hr +=1
    stats = Stats(total_reports,
        num_bp,
        num_hr,
        num_bp_alerts,
        num_hr_alerts)

    session.add(stats)

    session.commit()
    session.close()
    logger.info('HR recorded')
    return 201


def get_report_stats():
    logger.info('Request has been started')
    session = DB_SESSION()
    results = session.query(Stats)
    if not results:
        logger.error("Statistics does not exist")
        return 400

    #logger.debug(f"contents of python dictionary {results[-1].to_dict()}")
    logger.info("The request has been completed")
    session.close()
    result = results[-1].to_dict()
    logger.debug(f'{result}')
    return result, 200

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml",
      strict_validation = True,
      validate_responses = True)
if __name__ == "__main__":
   app.run(port=8080)

