import sqlite3

conn = sqlite3.connect('stats.sqlite')

c = conn.cursor()
c.execute('''
          CREATE TABLE stats
          (total_reports INTEGER NOT NULL,
           num_bp INTEGER NOT NULL,
           num_hr INTEGER NOT NULL,
           num_bp_alerts INTEGER NOT NULL,
           num_hr_alerts INTEGER NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

conn.commit()
conn.close()
