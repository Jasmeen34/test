from sqlalchemy import Column, Integer, String, DateTime
from base import Base
import datetime


class Stats(Base):
    """ BP and HR Statistics """

    __tablename__ = "stats"

    total_reports = Column(Integer, nullable=False)
    num_bp = Column(Integer, nullable=False)
    num_hr = Column(Integer, nullable=False)
    num_bp_alerts = Column(Integer, nullable=False)
    num_hr_alerts = Column(Integer, nullable=False)
    date_created = Column(DateTime, nullable=False, primary_key=True)

    def __init__(self, total_reports, num_bp, num_hr, num_bp_alerts, num_hr_alerts):
        """ Constructor """
        self.total_reports = total_reports
        self.num_bp = num_bp
        self.num_hr = num_hr
        self.num_bp_alerts = num_bp_alerts
        self.num_hr_alerts = num_hr_alerts
        self.date_created = datetime.datetime.now()

    def to_dict(self):
        """ Dictionary Representation of the BP/HR statistics """
        dict = {}
        dict['total_reports'] = self.total_reports
        dict['num_bp'] = self.num_bp
        dict['num_hr'] = self.num_hr
        dict['num_bp_alerts'] = self.num_bp_alerts
        dict['num_hr_alerts'] = self.num_hr_alerts

        return dict
